# == Class: file_perms
#
# Limits server access to linux_admin group (Jabil Standard)
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# === Examples
#
# === Authors
#
# sillgen <steven_illgen@jabil.com>
#
# === Copyright
#
# Copyright 2014 Jabil, unless otherwise noted.
#
class file_perms {
file { '/etc/passwd':
  ensure        => 'present',
  mode          => '0644',
  owner          => 'root',
  group         => 'root',
  }

file { '/etc/group':
  ensure        => 'present',
  mode          => '0644',
  owner          => 'root',
  group         => 'root',
  }

file { '/etc/shadow':
  ensure       => 'present',
  mode         => '0600',
  owner         => 'root',
  group        => 'root',
  }

file { '/etc/ghadow':
  ensure       => 'present',
  mode         => '0600',
  owner         => 'root',
  group        => 'root',
  }
}
